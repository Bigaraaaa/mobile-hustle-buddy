import { Component, OnInit } from '@angular/core';
import { ConnectionService } from '../services/connection.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  trainings;

  constructor(
    private connection: ConnectionService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.connection.getTrainings().subscribe((data) => {
      this.trainings = data;
    });
  }

  selectTraining(training) {
    this.router.navigate(['/training']);
    console.log(training);
  }

}
