import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {

  private trainingUrl = `http://localhost:8080/training/`;
  private exerciseUrl = `http://localhost:8080/exercise/`;

  constructor(
    private httpClient: HttpClient
  ) { }

  getTrainings() {
    return this.httpClient.get<any[]>(this.trainingUrl + `all`);
  }

  getExercises() {
    return this.httpClient.get<any>(this.exerciseUrl + `all`);
  }

}
