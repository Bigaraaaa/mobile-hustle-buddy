import { Component, OnInit, Input } from '@angular/core';
import { ConnectionService } from '../services/connection.service';

@Component({
  selector: 'app-explore-container',
  templateUrl: './explore-container.component.html',
  styleUrls: ['./explore-container.component.scss'],
})
export class ExploreContainerComponent {
  @Input() name: string;
}
