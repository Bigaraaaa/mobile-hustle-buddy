import { Component, OnInit } from '@angular/core';
import { ConnectionService } from 'src/app/services/connection.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.scss'],
})
export class TrainingComponent implements OnInit {
  exercises;

  constructor(
    private connectionService: ConnectionService,
    private location: Location
  ) { }

  ngOnInit() {
    this.connectionService.getExercises().subscribe(data => {
      this.exercises = data;
    });
  }

  back() {
    this.location.back();
  }

}
